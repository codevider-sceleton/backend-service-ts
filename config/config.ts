import * as dotenv from 'dotenv';
import * as path from 'path';
import { getOsEnv, getOsPaths, normalizePort, toNumber, toBool, getOsEnvOptional } from '../src/Util/EnvUtils';
import * as pkg from '../package.json';

const env = process.env;
const devMode = !env.NODE_ENV || env.NODE_ENV === 'dev';

const dotenvConfig = {
  path: path.join(process.cwd(), `.env${((devMode) ? '.dev' : '')}`),
};

dotenv.config(dotenvConfig);

export const config = {
  node: process.env.NODE_ENV || 'dev',
  isProduction: process.env.NODE_ENV === 'prod',
  isTest: process.env.NODE_ENV === 'test',
  isDevelopment: process.env.NODE_ENV === 'dev',
  projectRoot: __dirname + '/../',
  app: {
    name: getOsEnv('APP_NAME'),
    version: (pkg as any).version,
    description: (pkg as any).description,
    host: getOsEnv('APP_HOST'),
    schema: getOsEnv('APP_SCHEMA'),
    routePrefix: getOsEnv('APP_ROUTE_PREFIX'),
    port: normalizePort(process.env.PORT || getOsEnv('APP_PORT')),
    banner: toBool(getOsEnv('APP_BANNER')),
    dirs: {
      entities: getOsPaths('ENTITIES'),
      controllers: getOsPaths('CONTROLLERS'),
      middlewares: getOsPaths('MIDDLEWARES'),
      interceptors: getOsPaths('INTERCEPTORS'),
      repositories: getOsPaths('REPOSITORIES'),
      commands: getOsPaths('COMMANDS'),
      subscribers: getOsPaths('SUBSCRIBERS'),
    },
  },
  db: {
    type: getOsEnv('TYPEORM_CONNECTION') as any,
    host: getOsEnv('TYPEORM_HOST'),
    port: toNumber(getOsEnv('TYPEORM_PORT')),
    // username: getOsEnv('TYPEORM_USER'),
    // password: getOsEnv('TYPEORM_PASSWORD'),
    database: getOsEnv('TYPEORM_DATABASE'),
    synchronize: true,
    logging: getOsEnv('LOG_LEVEL') === 'debug',
  },

  jwt: {
    secret: getOsEnv('JWT_SECRET'),
    algorithm: getOsEnv('JWT_ALGORITHM'),
    expire: getOsEnv('JWT_EXPIRE'),
  },
  log: {
    level: getOsEnv('LOG_LEVEL'),
    json: toBool(getOsEnvOptional('LOG_JSON') as string),
    output: getOsEnv('LOG_OUTPUT'),
  },
  swagger: {
    enabled: toBool(getOsEnv('SWAGGER_ENABLED')),
    route: getOsEnv('SWAGGER_ROUTE'),
    username: getOsEnv('SWAGGER_USERNAME'),
    password: getOsEnv('SWAGGER_PASSWORD'),
  },
  monitor: {
    enabled: toBool(getOsEnv('MONITOR_ENABLED')),
    route: getOsEnv('MONITOR_ROUTE'),
    username: getOsEnv('MONITOR_USERNAME'),
    password: getOsEnv('MONITOR_PASSWORD'),
  },
};
