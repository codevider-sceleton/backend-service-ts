import { InputArgument, InputDefinition, InputInterface, InputOption } from './Input'
import { Application } from './Application'
import { CommandInterface } from './CommandInterface'

export class Command implements CommandInterface {

  private name: string | undefined;
  private readonly definition: InputDefinition;
  private application?: Application;
  protected static defaultName: string | undefined;

  constructor(name?: string) {
    this.definition = new InputDefinition();
    this.name = '';
    this.application = undefined;

    if (!name) {
      if (!Command.defaultName) {
        Command.defaultName = this.constructor.name;
      }

      const defaultName = Command.getDefaultName();
      if (defaultName) {
        this.setName(defaultName);
      }
    }
    this.configure();
  }

  /**
   * Get the default name of the command
   */
  public static getDefaultName(): string | undefined {
    return Command.defaultName;
  }

  /**
   * Executes the command
   */
  public execute(input: InputInterface): Promise<number> | number {
    return 0;
  }

  /**
   * Configure the command
   */
  public configure(): Promise<void> | void {
    return;
  }

  /**
   * Returns the current application
   */
  public getApplication(): Application | undefined {
    return this.application;
  }

  /**
   * Sets the application
   */
  public setApplication(application?: Application): this {
    this.application = application;

    return this;
  }

  /**
   * Runs the command
   */
  public run(input: InputInterface): Promise<number> | number {
    input.validate();
    return this.execute(input);
  }

  /**
   * Set the name of the command
   * @param name
   */
  public setName(name: string): this {
    this.name = name;
    return this;
  }

  /**
   * Get the command name
   */
  public getName(): string | undefined {
    return this.name;
  }

  /**
   * Add an argument to definition
   *
   * @param name
   * @param required
   * @param description
   * @param defaultValue
   */
  public addArgument(name: string, required: boolean = InputArgument.VALUE_OPTIONAL, description: string = '', defaultValue?: string): this {
    this.definition.addArgument(new InputArgument(name, description, required, defaultValue));
    return this;
  }

  /**
   * Add an option to definition
   *
   * @param name
   * @param shortcut
   * @param mode
   * @param description
   * @param defaultValue
   */
  public addOption(name: string, shortcut?: string | undefined, mode?: number, description: string = '', defaultValue?: string): this {
    this.definition.addOption(new InputOption(name, shortcut, description, mode, defaultValue));
    return this;
  }

  /**
   * Get the command definition
   */
  public getDefinition(): InputDefinition {
    return this.definition;
  }
}
