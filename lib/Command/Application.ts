// import 'reflect-metadata';
import Package from '../../package.json';
import { Container } from 'typedi';
import { importClassesFromDirectories } from './Util';
import { Command } from './Command';
import { InputInterface } from './Input';
import { config } from '../../config/config';
import { InputDefinition, Input } from './Input';
import { CommandInterface } from './CommandInterface';

export class Application {

  private definition: InputDefinition | undefined;
  private readonly version: string;
  private readonly options: [];
  private readonly arguments: [];
  private command: any;

  constructor() {
    this.version = Package.version;
    this.options = [];
    this.arguments = [];
    this.command = undefined;
    this.definition = undefined;
  }

  /**
   * Run this application
   */
  public async run(input?: InputInterface): Promise<number> {
    if (!input) {
      input = new Input();
    }
    // input.bind(command.getDefinition());
    this.find(this.getCommandName(input));

    if (!this.command) {
      console.log('Command not found!');
      return 0;
    }
    input.bind(this.command.getDefinition());
    return await this.command.run(input);
  }

  /**
   * getArguments
   */
  public getArguments(): [] {
    return this.arguments;
  }

  /**
   * getOptions
   */
  public getOptions(): [] {
    return this.options;
  }

  /**
   * getVersion
   */
  public getVersion(): string {
    return this.version;
  }

  /**
   * Gets the name of the command based on input.
   *
   * @return string|undefined
   */
  public getCommandName(input: InputInterface): string | undefined {
    return input.getFirstArgument();
  }

  public getDefinition(): InputDefinition | undefined {
    return this.definition;
  }

  public setDefinition(definition: InputDefinition): this {
    this.definition = definition;
    return this;
  }

  public getDefaultDefinition(): InputDefinition {
    return new InputDefinition();
  }

  /**
   * Get the command by name
   * @param name Name of the command
   */
  private find(name: string | undefined): Command | undefined {
    const patterns = [...config.app.dirs.commands, config.projectRoot + '/src/Security/Command/*.js'];
    const commands = importClassesFromDirectories(patterns);
    commands.forEach((command: any) => {
      const cmd = new command();

      // return the first matching command
      if (name === cmd.getName()) {
        // return this.command = cmd;
        return this.command = Container.get<CommandInterface>(command);
      }
    });

    return undefined;
  }
}
