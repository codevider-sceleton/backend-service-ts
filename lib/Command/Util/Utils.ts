import path from 'path';

/**
 * Loads all exported classes from the given directory.
 */
export const importClassesFromDirectories = (directories: any, formats: string[] = ['.js', '.ts']) => {
  const loadFileClasses = (exported: any, allLoaded: any) => {
    if (exported instanceof Function) {
      allLoaded.push(exported);
    } else if (exported instanceof Array) {
      exported.forEach((i) => {
        return loadFileClasses(i, allLoaded);
      });
    } else if (exported instanceof Object || typeof exported === 'object') {
      Object.keys(exported).forEach((key) => {
        return loadFileClasses(exported[key], allLoaded);
      });
    }
    return allLoaded;
  };

  const allFiles = directories.reduce((allDirs: any, dir: any) => {
    return allDirs.concat(require('glob').sync(path.normalize(dir)));
  }, []);

  // console.log(allFiles);

  const dirs = allFiles.filter((file: string) => {
      const dtsExtension = file.substring(file.length - 5, file.length);
      return formats.indexOf(path.extname(file)) !== -1 && dtsExtension !== '.d.ts';
    })
    .map((file: string) => {
      return require(file);
    });

  return loadFileClasses(dirs, []);
};

export const isNumber = (x: any) => {
  return typeof x === 'number'
    || (/^0x[0-9a-f]+$/i.test(x))
    || /^[-+]?(?:\d+(?:\.\d*)?|\.\d+)(e[-+]?\d+)?$/.test(x);
};
