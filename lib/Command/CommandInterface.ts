import { InputInterface } from './Input';
import { Application } from '.';
import { InputDefinition } from '.';

export interface CommandInterface {
  /**
   * Executes the command
   */
  execute(input: InputInterface): Promise<number> | number;

  /**
   * Configure the command
   */
  configure(): Promise<void> | void;

  /**
   * Returns the current application
   */
  getApplication(): Application | undefined;

  /**
   * Sets the application
   */
  setApplication(application ?: Application): this;

  /**
   * Runs the command
   */
  run(input: InputInterface): Promise<number> | number;

  /**
   * Set the name of the command
   * @param name
   */
  setName(name: string): this;

  /**
   * Get the command name
   */
  getName(): string | undefined;

  /**
   * Add an argument to definition
   *
   * @param name
   * @param required
   * @param description
   * @param defaultValue
   */
  addArgument(name: string, required: boolean, description: string, defaultValue?: string): this;

  /**
   * Add an option to definition
   *
   * @param name
   * @param shortcut
   * @param mode
   * @param description
   * @param defaultValue
   */
  addOption(name: string, shortcut?: string, mode?: number, description?: string, defaultValue?: string): this;

  /**
   * Get the command definition
   */
  getDefinition(): InputDefinition;
}
