import { InputArgument } from './InputArgument';
import { InputOption } from './InputOption';

export class InputDefinition {
  private readonly arguments: Map<string, InputArgument>;
  private readonly options: Map<string, InputOption>;
  private readonly shortcuts: Map<string, string>;
  private hasOptionalArgument: boolean;

  constructor(definition?: InputDefinition[]) {
    this.arguments = new Map();
    this.options = new Map();
    this.shortcuts = new Map();
    this.hasOptionalArgument = false;
  }

  /**
   * getArgument
   */
  public getArgument(name: string | number): InputArgument | undefined {
    if (!this.hasArgument(name)) {
      throw new Error(`Argument ${name} does not exist!`);
    }
    if (typeof name === 'number') {
      return [...this.arguments.entries()][name][1];

    }
    return this.arguments.get(name);
  }

  /**
   * getArguments
   */
  public getArguments(): Map<string, InputArgument> {
    return this.arguments;
  }

  /**
   * Returns true if an InputArgument object exists by name or position.
   */
  public hasArgument(name: string | number): boolean {
    if (typeof name === 'number') {
      return name in [...this.arguments.keys()];
    }
    return this.arguments.has(name);
  }

  /**
   * Get an option by its name
   * @param name Option name
   */
  public getOption(name: string): InputOption | undefined {
    const opt = this.options.get(name);
    if (!opt) {
      throw new Error(`Option ${name} does not exist!`);
    }
    return opt;
  }

  /**
   * Check if an option exist in definition
   * @param name 
   */
  public hasOption(name: string): boolean {
    return this.options.has(name);
  }

  /**
   * Return an array of InputOptions
   */
  public getOptions(): Map<string, InputOption> {
    return this.options;
  }

  /**
   * getOptionsObject
   */
  public getOptionsObject(): {} {
    const opts: {} = {};
    for (const [k, v] of this.options.entries()) {
      opts[k] = v;
    }
    return opts;
  }

  /**
   * getOptionByShortcut
   */
  public getOptionByShortcut(shortcut: string): InputOption | undefined {
    return this.getOption(this.shortcuts.get(shortcut) as string);
  }

  /**
   * shortcutToName
   */
  public shortcutToName(shortcut: string): string {
    if (!this.shortcuts.has(shortcut)) {
      throw new Error(`The option ${shortcut} does not exist!`);
    }

    return this.shortcuts.get(shortcut) as string;
  }

  /**
   * Add argument
   */
  public addArgument(argument: InputArgument): this {
    if (this.arguments.has(argument.getName())) {
      throw new Error(`An argument with name "${argument.getName()}" already exists.`);
    }
    if (argument.isRequired() && this.hasOptionalArgument) {
      throw new Error('A required argument cannot be set after an optional one!');
    }
    if (!argument.isRequired()) {
      this.hasOptionalArgument = true;
    }
    this.arguments.set(argument.getName(), argument);
    return this;
  }

  /**
   * Add option
   */
  public addOption(option: InputOption): this {
    if (this.options.has(option.getName())) {
      throw new Error(`An option with name "${option.getName()}" already exists.`);
    }
    if (this.shortcuts.has(option.getName() as string)) {
      throw new Error(`Another option shortcut exist for ${option.getShortcut()} name.`);
    }
    if (option.hasShortcut()) {
      if (this.options.has(option.getShortcut() as string)) {
        throw new Error(`An option named "${option.getShortcut()}" is already defined.`);
      }
      if (this.shortcuts.has(option.getShortcut() as string)) {
        throw new Error(`An option with shortcut "${option.getShortcut()}" already exist!`);
      }
    }
    this.options.set(option.getName(), option);
    if (option.hasShortcut()) {
        const syn = option.getShortcut() || option.getName();
        this.shortcuts.set(syn, option.getName());
    }

    return this;
  }

  /**
   * Get all shortcuts
   */
  public getShortcuts(): Map<string, string> {
    return this.shortcuts;
  }

  /**
   * hasShortcut
   */
  public hasShortcut(shortcut: string): boolean {
    return this.shortcuts.has(shortcut);
  }
}
