export class InputOption {

  private name: string;
  private readonly shortcut?: string | undefined;
  private readonly description: string;
  private mode: number;
  private defaultValue?: string | number | boolean | [];

  public static readonly VALUE_NONE: number = 1;
  public static readonly VALUE_REQUiRED: number = 2;
  public static readonly VALUE_OPTIONAL: number = 4;
  public static readonly VALUE_ARRAY: number = 8;

  constructor(name: string, shortcut?: string, description: string = '', mode: number = InputOption.VALUE_NONE, defaultValue?: string | number | boolean | []) {

    if (name === '') {
      throw new Error('An option name cannot be empty!');
    }

    if (shortcut && shortcut.length > 1) {
      throw new Error(`Invalid short option given ${shortcut}. An option shortcut requires only one character.`);
    }

    if (mode < 1 || mode > 8) {
      throw new Error(`Option mode "${mode}" is invalid! Option mode values are: 1 for flags, 2 for required and 3 for optional.`);
    }

    this.name = name;
    this.shortcut = shortcut;
    this.description = description;
    this.mode = mode;
    this.defaultValue = defaultValue;

    this.setDefault(defaultValue);
  }

  /**
   * Sets the default value.
   */
  public setDefault(defaultValue?: string | number | boolean | []): this {
    if (this.mode && this.mode === InputOption.VALUE_NONE && defaultValue) {
      throw new Error('Cannot set a default value if the option requires no value!');
    }
    this.defaultValue = this.acceptValue ? defaultValue : false;
    return this;
  }

  /**
   * Returns true if the option is required
   */
  public isRequired(): boolean {
    return this.mode === InputOption.VALUE_REQUiRED;
  }

  /**
   * Returns true if the option is optional
   */
  public isOptional(): boolean {
    return this.mode === InputOption.VALUE_OPTIONAL;
  }

  /**
   * isArray
   */
  public isArray(): boolean {
    return this.mode === InputOption.VALUE_ARRAY;
  }

  /**
   * Check if the option requires a value
   */
  public acceptValue(): boolean {
    return this.isRequired() || this.isOptional();
  }

  /**
   * Returns argument name
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Sets the option name
   * @param name Option name
   */
  public setName(name: string): this {
    this.name = name;
    return this;
  }

  /**
   * getMode
   */
  public getMode(): number {
    return this.mode;
  }

  /**
   * Set option mode (flag, optional or required)
   */
  public setMode(mode: number): this {
    this.mode = mode;
    return this;
  }

  /**
   * Returns argument description
   */
  public getDescription(): string {
    return this.description;
  }

  /**
   * Returns the default value
   */
  public getDefault(): string | number | boolean | [] | undefined {
    return this.defaultValue;
  }

  /**
   * Returns the option shortcut
   */
  public getShortcut(): string | undefined {
    return this.shortcut;
  }

  /**
   * Check if the option has a shortcut
   */
  public hasShortcut(): boolean {
    return this.shortcut !== undefined
      && this.shortcut !== ''
      && this.shortcut !== null;
  }

  /**
   * exist
   */
  public equals(option: InputOption): boolean {
    return this.getName() === option.getName()
      && this.getShortcut() === option.getShortcut();
  }
}
