import { InputDefinition } from './InputDefinition';
import { InputInterface } from './InputInterface';
import { InputArgument } from '..';

export class Input implements InputInterface {

  private readonly tokens: string[];
  private parsed: string[] | [];
  protected definition: InputDefinition;
  protected arguments: Map<string, string | undefined>;
  protected options: Map<string, any>;

  constructor(argv?: [], definition?: InputDefinition) {
    if (!argv) {
      argv = process.argv.slice(2) as [];
    }

    this.tokens = argv;

    if (!definition) {
      this.definition = new InputDefinition();
    } else {
      this.bind(definition);
      this.validate();
    }
  }

  /**
   * Get an argument by name
   * @param name Argument name
   */
  public getArgument(name: string): string | undefined {
    if (!this.definition.hasArgument(name)) {
      throw new Error(`Argument ${name} does not exist!`);
    }
    return this.arguments.get(name) || this.definition.getArgument(name)?.getDefault();
  }

  /**
   * Get the list of arguments
   */
  public getArguments(): {} {
    // merge input arguments with definition arguments
    const defaultArgs: {} = {};
    for (const [key, definedArgument] of this.definition.getArguments().entries()) {
      defaultArgs[key] = definedArgument.getDefault();
    }

    const args: {} = {};
    for (const [key, val] of this.arguments.entries()) {
      args[key] = val;
    }

    return { ...defaultArgs, ...args };
  }

  /**
   * Get an option by name
   * @param name Option name
   */
  public getOption(name: string): string | number | boolean | [] | undefined {
    if (!this.definition.hasOption(name)) {
      throw new Error(`Option ${name} does not exist!`);
    }

    return this.options.get(name) || this.definition.getOption(name)?.getDefault();
  }

  /**
   * Get options
   */
  public getOptions(): {} {
    // merge input options with definition arguments
    // the options will override option values
    const defaultOpts: {} = {};
    for (const [key, definedOption] of this.definition.getOptions().entries()) {
      defaultOpts[key] = definedOption.getDefault();
    }

    const opts: {} = {};
    for (const [key, val] of this.options.entries()) {
      opts[key] = val;
    }
    return { ...defaultOpts, ...opts };
  }

  /**
   * Sets the command definition
   * @param {InputDefinition} inputDefinition
   */
  public bind(inputDefinition: InputDefinition): void {
    this.arguments = new Map();
    this.options = new Map();
    this.definition = inputDefinition;

    this.parse();
  }

  /**
   * Parse all input options and arguments to definition types
   */
  public parse(): void {
    let parseOpts = true;
    this.parsed = this.tokens;
    // strip the command name if is set
    this.parsed.shift();

    let token: string | undefined = '';
    while (token !== undefined) {
      token = this.parsed.shift();
      if (token) {
        if (parseOpts && token === '') {
          this.parseArgument(token);
        } else if (parseOpts && token === '--') {
          parseOpts = false;
        } else if (parseOpts && token.indexOf('--') === 0) {
          this.parseLongOption(token);
        } else if (parseOpts && token[0] === '-' && token !== '-') {
          this.parseShortOption(token);
        } else {
          this.parseArgument(token);
        }
      }
    }
  }

  /**
   * Validate inputs
   */
  public validate(): void {
    const definition = this.definition;
    const missingArgs = [...definition.getArguments().keys()].filter((a) => {
      return !this.arguments.get(a) && definition.getArgument(a)?.isRequired();
    });
    if (missingArgs.length >= 1) {
      throw new Error(`Not enough arguments (missing: ${missingArgs.join(', ')})`);
    }
  }

  /**
   * The first argument is the command name or an option
   */
  public getFirstArgument(): string | undefined {
    if (this.tokens.length >= 1) {
      return this.tokens[0];
    }
    return;
  }

  /**
   * Parse an argument
   * @param token
   */
  private parseArgument(token: string): void {
    const c = this.arguments.size;
    if (this.definition.hasArgument(c)) {
      const arg = this.definition.getArgument(c) as InputArgument;
      this.arguments.set(arg.getName(), token);
    } else {
      const all = this.definition.getArguments();
      if (all.size) {
        throw new Error(`Too many arguments, expected arguments are ${[...this.arguments.keys()].join(' ')}`);
      }
      throw new Error(`No argument expected, got ${token}`);
    }
  }

  /**
   * Parse long option
   * @param token
   */
  private parseLongOption(token: string): void {
    const name = token.substring(2); // strip -- from token
    const pos = name.indexOf('=');
    let val: string | undefined;
    if (pos !== -1) {
      val = name.substring(pos + 1);
      if (val.length === 0) {
        this.parsed.unshift(val as never);
      }
      this.addLongOption(name.substring(0, pos), val);
    } else {
      this.addLongOption(name, undefined);
    }
  }

  /**
   * Add value to long option
   * @param name
   * @param value
   */
  private addLongOption(name: string, value: string | number | boolean | undefined): void {
    if (!this.definition.hasOption(name)) {
      throw new Error(`The "--${name}" option does not exist.`);
    }

    const option = this.definition.getOption(name);

    if (value && !option?.acceptValue()) {
      throw new Error(`Option "--${name}" does not accept a value.`);
    }

    if (['', undefined].includes(value as string) && option?.acceptValue() && this.parsed.length > 0) {
      const next = this.parsed.shift();
      if ((next && next[0] !== '-') || ['', undefined].includes(next)) {
        value = next;
      } else {
        this.parsed.unshift(next as never);
      }
    }

    if (!value) {
      if (option?.isRequired()) {
        throw new Error(`Option "--${name}" requires a value.`);
      }

      if (!option?.isArray() && !option?.isOptional()) {
        value = true;
      }
    }

    if (option?.isArray()) {
      this.options.set(name, [value]);
    } else {
      this.options.set(name, value);
    }
  }

  /**
   * parseShortOption
   */
  private parseShortOption(token: string): void {
    const name = token.substring(1);
    if (name.length > 1) {
      if (this.definition.hasShortcut(name[0]) && this.definition.getOptionByShortcut(name[0])?.acceptValue()) {
        this.addShortOption(name[0], name.substring(1));
      } else {
        this.parseShortOptionSet(name);
      }
    } else {
      this.addShortOption(name, undefined);
    }
  }

  /**
   * Parse short option set ex. -abc
   * @param name
   */
  private parseShortOptionSet(name: string): void {
    const len = name.length;
    for (let i = 0; i < len; i++) {
      if (!this.definition.hasShortcut(name[i])) {
        throw new Error(`The "-${name[i]}" option does not exist.`);
      }

      const option = this.definition.getOptionByShortcut(name[i]);

      if (option) {
        if (option.acceptValue()) {
          this.addLongOption(option.getName(), i === len - 1 ? undefined : name.substring(i + 1));
          break;
        } else {
          this.addLongOption(option.getName(), undefined);
        }
      }
    }
  }

  /**
   * Adds a short option value.
   *
   * @param shortcut
   * @param value
   */
  private addShortOption(shortcut: string, value: any): void {
    if (!this.definition.hasShortcut(shortcut)) {
      throw new Error(`Option "-${shortcut}" does not exist.`);
    }
    const option = this.definition.getOptionByShortcut(shortcut);
    this.addLongOption(option?.getName() as string, value);
  }

}
