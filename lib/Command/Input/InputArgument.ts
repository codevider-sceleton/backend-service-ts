export class InputArgument {
  private readonly name: string;
  private readonly description: string;
  private readonly required: boolean;
  private defaultValue?: string;

  public static readonly VALUE_REQUIRED = true;
  public static readonly VALUE_OPTIONAL = false;

  constructor(name: string, description: string = '', required: boolean = InputArgument.VALUE_OPTIONAL, defaultValue?: string) {
    if (name === '') {
      throw new Error('Argument can not have an empty name!');
    }

    this.name = name;
    this.description = description;
    this.required = required;
    this.defaultValue = defaultValue;

    this.setDefault(defaultValue);
  }

  /**
   * Sets the default value.
   */
  public setDefault(defaultValue?: string): this {
    if (this.required && defaultValue) {
      throw new Error('Cannot set a default value if the argument is required!');
    }
    this.defaultValue = defaultValue;
    return this;
  }

  /**
   * Returns true if the argument is required
   */
  public isRequired(): boolean {
    return this.required;
  }

  /**
   * Returns argument name
   */
  public getName(): string {
    return this.name;
  }

  /**
   * Returns argument description
   */
  public getDescription(): string {
    return this.description;
  }

  /**
   * Returns the default value
   */
  public getDefault(): string | undefined {
    return this.defaultValue;
  }
}
