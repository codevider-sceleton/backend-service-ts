import { InputDefinition } from './InputDefinition';

export interface InputInterface {
  getArgument(name: string): string | undefined;

  getArguments(): {};

  getOption(name: string): string | number | boolean | [] | undefined;

  getOptions(): {};

  bind(inputDefinition: InputDefinition): void;

  validate(): void;

  /**
   * Get the first argument.
   * The firs argument is the command name or an option
   */
  getFirstArgument(): string | undefined;
}
