import { config } from '../config/config';
import { Logger } from './Logger';
import chalk from 'chalk';

export function banner(log: Logger): void {
  const route = () => `${config.app.schema}://${config.app.host}:${config.app.port}`;
  if (config.app.banner) {
    log.info(``);
    log.info('App is running');
    log.info(`To shut it down, press <CTRL> + C at any time.`);
    log.info(``);
    log.info('-------------------------------------------------------');
    log.info(`Environment  : ${config.node}`);
    log.info(`Version      : ${config.app.version}`);
    log.info(``);
    log.info(chalk.green(`API Info     : ${route()}${config.app.routePrefix}`));
    if (config.swagger.enabled) {
      log.info(chalk.green(`Swagger      : ${route()}${config.swagger.route}`));
    }
    if (config.monitor.enabled) {
      log.info(chalk.green(`Monitor      : ${route()}${config.monitor.route}`));
    } 
    log.info('-------------------------------------------------------');
    log.info('');
  } else {
    log.info(`Application is up and running on ${chalk.green(route())}.`);
  }
}
