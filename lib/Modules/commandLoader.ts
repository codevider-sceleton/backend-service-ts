import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { config } from '../../config/config';
import { Input, Application } from '../Command';

export const commandLoader: MicroframeworkLoader = async (settings?: MicroframeworkSettings) => {
  const input = new Input();
  const application = new Application();
  
  await application.run(input);
};
