import { useContainer as classValidatorUseContainer } from 'class-validator';
import { MicroframeworkLoader } from 'microframework';
import { useContainer as routingUseContainer } from 'routing-controllers';
import { Container } from 'typedi';
import { useContainer as ormUseContainer } from 'typeorm';
// import { useContainer as commandUseContainer } from '../Command';

export const containerLoader: MicroframeworkLoader = () => {

    /**
     * Setup routing-controllers to use typedi container and typeorm.
     */
    ormUseContainer(Container);
    routingUseContainer(Container);
    classValidatorUseContainer(Container);
    // commandUseContainer(Container);
};
