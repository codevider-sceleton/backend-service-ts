import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { getConnectionOptions, createConnection, ConnectionOptions } from 'typeorm';
import { config } from '../../config/config';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

export const ormLoader: MicroframeworkLoader = async (settings?: MicroframeworkSettings) => {
  
  const loadedConnectionOptions: ConnectionOptions = await getConnectionOptions();
  const userEntity = config.projectRoot + '/src/Security/User.js';
  const connectionOptions = {
    ...loadedConnectionOptions,
    ...config.db,
    useUnifiedTopology: true,
    entities: [...config.app.dirs.entities, userEntity],
    namingStrategy: new SnakeNamingStrategy(),
  };

  const conn = await createConnection(connectionOptions);

  if (settings) {
    settings.setData('connection', conn);
    settings.onShutdown(() => conn.close());
  }
};
