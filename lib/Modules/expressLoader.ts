import { Application, Request, Response } from 'express';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
import { createExpressServer } from 'routing-controllers';
import { authorizationChecker } from '../../src/Security/Authentication/authorizationChecker';
import { currentUserChecker } from '../../src/Security/Authentication/currentUserChecker';
import { config } from '../../config/config';
import { Connection } from 'typeorm';

export const expressLoader: MicroframeworkLoader = (settings?: MicroframeworkSettings) => {
  if (settings) {
    const connection: Connection = settings.getData('connection');
    
    /**
     * We create a new express server instance.
     * We could have also use useExpressServer here to attach controllers to an existing express instance.
     */
    const expressApp: Application = createExpressServer({
      cors: {
        credentials: true,
        origin: true,
      },
      classTransformer: true,
      routePrefix: config.app.routePrefix,
      defaultErrorHandler: true,
      /**
       * We can add options about how routing-controllers should configure itself.
       * Here we specify what controllers should be registered in our express server.
       */
      controllers: config.app.dirs.controllers,
      middlewares: config.app.dirs.middlewares,
      interceptors: config.app.dirs.interceptors,
      validation: false,
      /**
       * Authorization features
       */
      authorizationChecker: authorizationChecker(connection),
      currentUserChecker: currentUserChecker(connection),
    });

    // Run application to listen on given port
    if (!config.isTest) {
      const server = expressApp.listen(config.app.port);
      settings.setData('express_server', server);
    }

    // application entrypoint
    expressApp.get(
      config.app.routePrefix,
      (req: Request, res: Response) => {
        return res.json({
          name: config.app.name,
          version: config.app.version,
          description: config.app.description,
        });
      }
    );
    // Here we can set the data for other loaders
    settings.setData('express_app', expressApp);
  }
};
