// import { defaultMetadataStorage as classTransformerMetadataStorage } from 'class-transformer/storage';
// import { getFromContainer, MetadataStorage } from 'class-validator';
// import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
// import basicAuth from 'express-basic-auth';
// import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';
// import { getMetadataArgsStorage } from 'routing-controllers';
// import { routingControllersToSpec } from 'routing-controllers-openapi';
// import * as swaggerUi from 'swagger-ui-express';
// import { config } from '../../config/config';

// export const swaggerLoader: MicroframeworkLoader = (settings?: MicroframeworkSettings) => {
//   if (settings && config.swagger.enabled) {
//     const expressApp = settings.getData('express_app');

//     const { validationMetadatas } = getFromContainer(
//       MetadataStorage
//     ) as any;

//     const schemas = validationMetadatasToSchemas(validationMetadatas, {
//       classTransformerMetadataStorage,
//       refPointerPrefix: '#/components/schemas/',
//     });

//     const swaggerFile = routingControllersToSpec(
//       getMetadataArgsStorage(),
//       {},
//       {
//         components: {
//           schemas,
//           securitySchemes: {
//             basicAuth: {
//               type: 'http',
//               scheme: 'jwt',
//             },
//           },
//         },
//       }
//     );

//     // Add npm infos to the swagger doc
//     swaggerFile.info = {
//       title: config.app.name,
//       description: config.app.description,
//       version: config.app.version,
//     };

//     swaggerFile.servers = [
//       {
//         url: `${config.app.schema}://${config.app.host}:${config.app.port}${config.app.routePrefix}`,
//       },
//     ];

//     expressApp.use(
//       config.swagger.route,
//       config.swagger.username ? basicAuth({
//         users: {
//           [`${config.swagger.username}`]: config.swagger.password,
//         },
//         challenge: true,
//       }) : (req, res, next) => next(),
//       swaggerUi.serve,
//       swaggerUi.setup(swaggerFile)
//     );
//   }
// };
