// import glob from 'glob';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework';

import { config } from '../../config/config';

/**
 * eventDispatchLoader
 * ------------------------------
 * This loads all the created subscribers into the project, so we do not have to
 * import them manually
 */
export const eventDispatchLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
  if (settings) {
    const patterns = [...config.app.dirs.subscribers, config.projectRoot + '/src/Security/EventSubscriber/*.js'];
    patterns.forEach((pattern) => {
      require('glob')(pattern, (err: any, files: string[]) => {
        for (const file of files) {
          require(file);
        }
      });
    });
  }
};
