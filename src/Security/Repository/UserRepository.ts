import { EntityRepository, MongoRepository } from 'typeorm';

import { User } from '../User';

@EntityRepository(User)
export class UserRepository extends MongoRepository<User> { }
