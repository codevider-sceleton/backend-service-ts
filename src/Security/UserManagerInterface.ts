import { User } from './User';

export interface UserManagerInterface {
    /**
     * Get the logged in user.
     *
     * @return User
     */
    getUser(): User;

    /**
     * Deletes a user.
     *
     * @param Promise<void>
     */
    deleteUser(id: number): Promise<void>;

    /**
     * Finds one user by the given criteria.
     *
     * @param {object} criteria
     *
     * @return Promise<User | undefined>
     */
    findUserBy(criteria: object): Promise<User | undefined>;

    /**
     * Find a user by its id.
     *
     * @param {string | number} id
     *
     * @return Promise<User | undefined>
     */
    findUserById(id: string | number): Promise<User | undefined>;

    /**
     * Find a user by its username.
     *
     * @param {string} username
     *
     * @return Promise<User | undefined>
     */
    findUserByUsername(username: string): Promise<User | undefined>;

    /**
     * Finds a user by its email.
     *
     * @param {string} email
     *
     * @return Promise<User | undefined>
     */
    findUserByEmail(email: string): Promise<User | undefined>;

    /**
     * Finds a user by its username or email.
     *
     * @param {string} usernameOrEmail
     *
     * @return Promise<User | undefined>
     */
    findUserByUsernameOrEmail(usernameOrEmail: string): Promise<User | undefined>;

    /**
     * Finds a user by its confirmationToken.
     *
     * @param {string} token
     *
     * @return Promise<User | undefined>
     */
    findUserByConfirmationToken(token: string): Promise<User | undefined>;

    /**
     * Returns a collection with all user instances.
     *
     * @return Promise<User[]>
     */
    findUsers(): Promise<User[]>;

    /**
     * Reloads a user.
     *
     * @param {User} $user
     */
    // reloadUser(user: User): Promise<void>;

    /**
     * Save a user resource.
     *
     * @param {User} user
     */
    save(user: User): Promise<void>;

    /**
     * Updates a user password if a plain password is set.
     *
     * @param {User} user
     */
    updatePassword(user: User): Promise<void>;
}
