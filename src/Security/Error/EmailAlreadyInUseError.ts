import { HttpError } from 'routing-controllers';

export class EmailAlreadyInUseError extends HttpError {
  public operationName: string;
  public args: any[];

  constructor(operationName: string, args: any[] = []) {
    super(400);
    Object.setPrototypeOf(this, EmailAlreadyInUseError.prototype);
    this.operationName = operationName;
    this.args = args; // can be used for internal logging
  }

  public toJSON(): object {
    return {
      status: this.httpCode,
      failedOperation: this.operationName,
    };
  }
}
