import { HttpError } from 'routing-controllers';

export class UsernameAlreadyInUseError extends HttpError {
  public operationName: string;
  public args: any[];

  constructor(operationName: string, args: any[] = []) {
    super(400);
    Object.setPrototypeOf(this, UsernameAlreadyInUseError.prototype);
    this.operationName = operationName;
    this.args = args; // can be used for internal logging
  }

  public toJSON(): object {
    return {
      status: this.httpCode,
      message: 'Username already in use!',
      failedOperation: this.operationName,
    };
  }
}
