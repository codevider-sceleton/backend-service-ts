import { Exclude, Type, Transform } from 'class-transformer';
import { IsNotEmpty, IsEmail, MaxLength, MinLength, IsArray } from 'class-validator';
import { Entity, Column, ObjectID, ObjectIdColumn, Index } from 'typeorm';
// import IsUnique from './Validator/IsUnique';

@Entity()
@Index('search_idx', ['username', 'firstName', 'lastName'])
export class User {

  public static readonly ROLE_USER = 'ROLE_USER';
  public static readonly ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

  @ObjectIdColumn()
  @Type(() => ObjectID)
  @Transform(value => value.toString(), {toPlainOnly: true})
  public id: ObjectID | string;

  @IsNotEmpty({ groups: ['registration', 'admin:create'] })
  @MaxLength(25, { groups: ['registration', 'admin:create'] })
  @MinLength(3, { groups: ['registration', 'admin:create'] })
  // @IsUnique({ groups: ['registration', 'admin:create'], message: 'Username already in use!' })
  @Column({ length: 25 })
  @Index('unique_username_idx', { unique: true })
  public username: string;

  @Column({ nullable: true, length: 75 })
  @MaxLength(75, { groups: ['registration', 'profile:edit'] })
  @MinLength(2, { groups: ['registration', 'profile:edit'] })
  public firstName: string;

  @Column({ nullable: true, length: 75 })
  @MaxLength(75, { groups: ['registration', 'profile:edit'] })
  @MinLength(2, { groups: ['registration', 'profile:edit'] })
  public lastName: string;

  @IsNotEmpty({ groups: ['registration', 'login', 'admin:create'] })
  @IsEmail()
  @MaxLength(150, { groups: ['registration'] })
  // @IsUnique({ groups: ['registration', 'admin:create'], message: 'Email already in use!' })
  @Column({ length: 150 })
  @Index('unique_email_idx', { unique: true })
  public email: string;

  @IsNotEmpty({ groups: ['registration', 'login'] })
  @Column()
  @Exclude({ toPlainOnly: true })
  public password: string;

  /**
   * Plain password. Used for model validation. Must not be persisted.
   */
  @IsNotEmpty({ groups: ['profile:changePassword'] })
  @Exclude()
  public plainPassword: string | undefined;

  /**
   * Old password. Used for model validation. Must not be persisted.
   */
  @IsNotEmpty({ groups: ['profile:changePassword'] })
  @Exclude()
  public oldPassword: string | undefined;

  @IsArray({ groups: ['admin:create', 'admin:roles'] })
  @Column({ type: 'json' })
  public roles: string[] = [];

  @Column({ default: true, update: false })
  @Index('is_active_idx')
  public enabled: boolean;

  @Column({ default: false })
  @Index('is_locked_idx')
  public locked: boolean;

  @Column({ type: 'varchar', length: 64, nullable: true })
  @Exclude()
  @Index('confirm_idx')
  public confirmationToken: string | undefined;

  @Column({ type: 'datetime', nullable: true })
  @Exclude()
  public passwordRequestedAt: string | undefined;

  @Column({ type: 'datetime', nullable: true })
  public lastLogin: string;

  constructor() {
    this.roles = this.getRoles();
  }

  public getId(): string {
    return this.id.toString();
  }

  /**
   * Removes sensitive data from the user.
   *
   * This is important if, at any given point, sensitive information like
   * the plain-text password is stored on this object.
   */
  public eraseCredentials(): void {
    this.plainPassword = undefined;
    this.oldPassword = undefined;
  }

  public getRoles(): string[] {
    if (this.roles.length === 0) {
      this.roles.push(User.ROLE_USER);
    }

    return this.roles;
  }

  public addRole(role: string): this {
    const newRole: string = role.toUpperCase();

    if (newRole === User.ROLE_USER) {
      return this;
    }

    if (!this.hasRole(role)) {
      this.roles.push(role);
    }

    return this;
  }

  public hasRole(role: string): boolean {
    return this.roles.includes(role.toUpperCase());
  }

  public removeRole(role: string): this {
    if (this.hasRole(role)) {
      this.roles.splice(this.roles.indexOf(role), 1);
    }

    return this;
  }

  /**
   * Set current user as super admin
   */
  public setSuperAdmin(boolean: boolean = true): this {
    if (boolean) {
      this.addRole(User.ROLE_SUPER_ADMIN);
    } else {
      this.removeRole(User.ROLE_SUPER_ADMIN);
    }

    return this;
  }

  /**
   * Checks if current user is super admin
   */
  public isSuperAdmin(): boolean {
    return this.hasRole(User.ROLE_SUPER_ADMIN);
  }

  public toString(): string {
    return `${this.username} (${this.email})`;
  }
}
