import { registerDecorator, ValidationOptions } from 'class-validator';
import { IsUniqueConstraint } from './Constraints/IsUniqueConstraint';

/**
 * Check if entity field is unique and does not exist in database
 * Perform a database select
 *
 * @param {ValidationOptions} validationOptions
 */
export default (validationOptions?: ValidationOptions): any => {
  return (object: object, propertyName: string): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsUniqueConstraint,
    });
  };
};
