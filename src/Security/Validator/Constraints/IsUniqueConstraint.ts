import { UserManager } from '../../UserManager';
import { Inject } from 'typedi';
import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';

@ValidatorConstraint({ async: true })
export class IsUniqueConstraint implements ValidatorConstraintInterface {

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @Inject(type => UserManager)
  private userManager: UserManager;

  public async validate(field: string, args: ValidationArguments): Promise<boolean> {
    const prop = args.property;
    let record: object | undefined = undefined;

    try {
      if (prop === 'username') {
        record = await this.userManager.findUserByUsername(field);
      } else if (prop === 'email') {
        record = await this.userManager.findUserByEmail(field);
      }

      if (!record) {
        return true;
      }

    } catch (e) {
      console.log(e.message);
    }

    return false;
  }
}
