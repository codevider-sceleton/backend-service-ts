import { User } from '../User';
import { TokenInterface } from './TokenInterface';

export class Token implements TokenInterface {

  private rawToken?: string;
  private user?: User | string;
  private readonly roles: string[];
  private authenticated: boolean;

  constructor(roles: string[], user?: User, rawToken?: string) {
    this.roles = roles;
    this.user = user;
    this.setRawToken(rawToken);
    this.authenticated = false;
  }

  /**
   * @inheritdoc
   */
  public getRawToken(): string | undefined {
    return this.rawToken;
  }

  /**
   * @inheritdoc
   */
  public setRawToken(rawToken?: string): this {
    this.rawToken = rawToken;
    return this;
  }

  /**
   * @inheritdoc
   */
  public toString(): string {
    return `${this.user?.toString()}`;
  }

  /**
   * Returns the user roles.
   *
   * @return string[] An array of roles
   */
  public getRoles(): string[] {
    return this.roles;
  }

  /**
   * Returns a user representation.
   *
   * @return {User|string} Can be a UserInterface instance, an object implementing a toString method,
   *                       or the username as string
   *
   */
  public getUser(): User | string | undefined {
    return this.user;
  }

  /**
   * Sets the user in the token.
   *
   * The user can be a User instance, or an object implementing
   * a toString method or the username as string.
   *
   * @param {User|string} user The user
   */
  public setUser(user: User): this {
    this.user = user;
    return this;
  }

  /**
   * Returns the username.
   *
   * @return {string}
   */
  public getUsername(): string {
    return this.user as string;
  }

  /**
   * Returns whether the user is authenticated or not.
   *
   * @return {boolean} true if the token has been authenticated, false otherwise
   */
  public isAuthenticated(): boolean {
    return this.authenticated;
  }

  /**
   * Sets the authenticated flag.
   *
   * @param {boolean} isAuthenticated The authenticated flag
   */
  public setAuthenticated(isAuthenticated: boolean): this {
    this.authenticated = isAuthenticated;
    return this;
  }
}
