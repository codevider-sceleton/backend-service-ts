import { TokenExtractorInterface } from './TokenExtractorInterface';
import { Request } from 'express';

export class AuthorizationHeaderTokenExtractor implements TokenExtractorInterface {

  private readonly headerName: string;
  private readonly prefix?: string;

  constructor(prefix: string = 'Bearer', headerName: string = 'authorization') {
    this.headerName = headerName;
    this.prefix = prefix;
  }

  public extract(request: Request): string | undefined {
    const authorizationHeader = request.get(this.headerName);
    if (!authorizationHeader) {
      return;
    }

    if (!this.prefix) {
      return authorizationHeader;
    }

    const headerParts = authorizationHeader.split(' ');

    if (!(headerParts.length === 2 && headerParts[0].toLowerCase() === this.prefix.toLowerCase())) {
      return;
    }

    return headerParts[1];
  }
}
