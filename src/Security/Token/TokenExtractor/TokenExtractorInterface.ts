import { Request } from 'express';

export interface TokenExtractorInterface {
  /**
   * @param {Request} request 
   */
  extract(request: Request): string | undefined;
}
