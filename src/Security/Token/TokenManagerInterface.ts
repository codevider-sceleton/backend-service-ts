import { User } from '..';
import { TokenInterface } from './TokenInterface';

export interface TokenManagerInterface {
  /**
   * Create the JWT token
   *
   * @param {User} user
   *
   * @return string The JWT token
   */
  create(user: User): string;

  /**
   * Decode the JWT token
   *
   * @param {TokenInterface} token
   *
   * @return object | boolean The JWT token payload or false if an error occurs
   */
  decode(token: TokenInterface): object | boolean;

  /**
   * Sets the field used as identifier to load an user from a JWT payload.
   *
   * @param field
   */
  setUserIdentityField(field: string): this;

  /**
   * Returns the field used as identifier to load an user from a JWT payload.
   *
   * @return string
   */
  getUserIdentityField(): string;

  /**
   * Returns the claim used as identifier to load an user from a JWT payload.
   *
   * @return string
   */
  getUserIdClaim(): string;
}
