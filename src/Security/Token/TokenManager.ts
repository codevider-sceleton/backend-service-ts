import { User } from '..';
import { TokenInterface } from './TokenInterface';
import jwt, { SignOptions, Algorithm } from 'jsonwebtoken';
import {config} from '../../../config/config';
import { Service } from 'typedi';
import { Token } from './Token';

@Service()
export class TokenManager {

  private userIdentityField: string;

  constructor() {
    // defaults to username
    this.userIdentityField = 'email';
  }

  /**
   * @inheritdoc
   */
  public create(user: User): string {
    const payload: {} = {
      [this.userIdentityField]: user[this.userIdentityField],
      'roles': user.getRoles(),
    };

    const options: SignOptions = {
      algorithm: config.jwt.algorithm as Algorithm,
      expiresIn: config.isProduction ? config.jwt.expire : '1year',
    };

    return jwt.sign(payload, config.jwt.secret, options);
  }

  /**
   * @inheritdoc
   */
  public decode(token: TokenInterface | string): object | boolean {
    let rawToken: TokenInterface | string;
    if (token instanceof Token) {
      rawToken = token.getRawToken() as string;
    } else {
      rawToken = token;
    }

    const decoded = jwt.verify(rawToken as string, config.jwt.secret);
    if (!decoded) {
      return false;
    }
    return decoded as object;
  }

  /**
   * @inheritdoc
   */
  public setUserIdentityField(field: string): this {
    this.userIdentityField = field;
    return this;
  }

  /**
   * @inheritdoc
   */
  public getUserIdentityField(): string {
    return this.userIdentityField;
  }
}
