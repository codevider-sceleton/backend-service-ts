import { TokenInterface } from './TokenInterface';
import { Service } from 'typedi';

@Service()
export class TokenStorage {

  private token: TokenInterface | undefined;
  
  constructor() {
    this.token = undefined;
  }
  
  /**
   * @inheritdoc
   */
  public getToken(): TokenInterface | undefined {
    return this.token;
  }

  /**
   * @inheritdoc
   */
  public setToken(token?: TokenInterface): this {
    this.token = token;
    return this;
  }
}
