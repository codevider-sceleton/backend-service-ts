import { TokenInterface } from './TokenInterface';

export interface TokenStorageInterface {
  /**
   * Returns the current security token.
   *
   * @return TokenInterface|undefined A TokenInterface instance or
   *         undefined if no authentication information is available
   */
  getToken(): TokenInterface | undefined;

  /**
   * Sets the authentication token.
   *
   * @param {TokenInterface} token A TokenInterface token, or undefined if no further authentication information should be stored
   */
  setToken(token?: TokenInterface): this;
}
