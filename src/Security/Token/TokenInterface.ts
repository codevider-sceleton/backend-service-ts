import { User } from '../User';

export interface TokenInterface {
  /**
   * Returns a string representation of the Token.
   *
   * This is only to be used for debugging purposes.
   *
   * @return string
   */
  toString(): string;

  /**
   * Returns the user roles.
   *
   * @return string[] An array of roles
   */
  getRoles(): string[];

  /**
   * Returns the jwt token.
   *
   * @return string The user credentials
   */
  getRawToken(): string | undefined;

  /**
   * Set the jwt token
   * @param {string} rawToken
   */
  setRawToken(rawToken?: string): this;

  /**
   * Returns a user representation.
   *
   * @return {User|string} Can be a UserInterface instance, an object implementing a toString method,
   *                       or the username as string
   */
  getUser(): User | string | undefined;

  /**
   * Sets the user in the token.
   *
   * The user can be a User instance, or an object implementing
   * a toString method or the username as string.
   *
   * @param {User|string} user The user
   */
  setUser(user: User): this;

  /**
   * Returns the username.
   *
   * @return {string}
   */
  getUsername(): string;

  /**
   * Returns whether the user is authenticated or not.
   *
   * @return {boolean} true if the token has been authenticated, false otherwise
   */
  isAuthenticated(): boolean;

  /**
   * Sets the authenticated flag.
   *
   * @param {boolean} isAuthenticated The authenticated flag
   */
  setAuthenticated(isAuthenticated: boolean): this;
}
