import { User, UserManagerInterface, UserManager } from '..'
import { Inject, Service } from 'typedi'
import { TokenManager } from '../Token/TokenManager'
import { TokenManagerInterface } from '../Token/TokenManagerInterface'
import { UnauthorizedError } from 'routing-controllers'
import { PasswordUpdater } from '../Util'

@Service()
export class Authentication {

  constructor(
    @Inject(type => UserManager) private userManager: UserManagerInterface,
    @Inject(type => TokenManager) private tokenManager: TokenManagerInterface
  ) { }

  /**
   * Authenticate a user
   * @param credentials
   */
  public async authenticate(credentials: User): Promise<string> {
    const user = await this.userManager.findUserByEmail(credentials.email);
    if (!user) {
      throw new UnauthorizedError('Invalid credentials.');
    }

    const isValidPassword = await PasswordUpdater.checkPassword(user, credentials.password);

    if (!isValidPassword) {
      throw new UnauthorizedError('Invalid credentials.');
    }

    return this.tokenManager.create(user);
  }

  /**
   * Register a user
   *
   * @param payload User payload
   */
  public async register(payload: User): Promise<User> {
    if (!payload.plainPassword) {
      payload.plainPassword = payload.password;
    }

    await this.userManager.save(payload);
    return payload;
  }
}
