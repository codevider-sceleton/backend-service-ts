import { Request } from 'express'
import { Action } from 'routing-controllers'
import { Container } from 'typedi'
import { Connection } from 'typeorm'
import { Logger } from '../../../lib/Logger'
import { Token } from '../Token/Token'
import { TokenManager } from '../Token/TokenManager'
import { TokenExtractorInterface } from '../Token/TokenExtractor/TokenExtractorInterface'
import { AuthorizationHeaderTokenExtractor } from '../Token/TokenExtractor/AuthorizationHeaderTokenExtractor'
import { UserManager } from '../UserManager'
import { User } from '..'

export const authorizationChecker = (connection: Connection): (action: Action, roles: string[]) => Promise<boolean> | boolean => {
  const log = new Logger('Security');
  const tokenManager = Container.get<TokenManager>(TokenManager);
  const userManager = Container.get<UserManager>(UserManager);

  const token = async (request: Request): Promise<Token | undefined > => {
    const tokenExtractor: TokenExtractorInterface = new AuthorizationHeaderTokenExtractor();
    const extractedToken = tokenExtractor.extract(request);

    if (extractedToken) {
      try {
        const tokenPayload: any = tokenManager.decode(extractedToken);
        const user = await userManager.findUserByEmail(tokenPayload.email);

        return new Token(user?.getRoles() as string[], user, extractedToken);
      } catch (err) {
        log.error(err.message);
        throw err;
      }
    }
  };

  return async (action: Action, roles: string[]): Promise<boolean> => {
    const request: Request = action.request;
    // const response: Response = action.response;
    const loadedToken = await token(request);
    const user = loadedToken?.getUser() as User;

    if (!user) {
      return false;
    }

    // check roles
    for (const role of roles) {
      if (!user.hasRole(role)) {
        log.info(`User with role ${role} not granted to view this resource`);
        return false;
      }
    }
    return true;
  };
};
