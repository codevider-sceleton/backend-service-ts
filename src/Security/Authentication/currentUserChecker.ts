import { Action } from 'routing-controllers';
import { Connection } from 'typeorm';
import { User } from '..';

export const currentUserChecker = (connection: Connection): (action: Action) => Promise<User | undefined> => {
  return async (action: Action): Promise<User | undefined> => {
    return action.request.user;
  };
};
