import { EventSubscriber, On } from 'event-dispatch';

import { Logger } from '../../../lib/Logger';
import { User } from '../User';
import { Event } from './Event';
import { Service } from 'typedi';

const log = new Logger(__filename);

@Service()
@EventSubscriber()
export class UserSubscriber {

  @On(Event.ON_USER_BEFORE_CREATE)
  public onUserBeforeCreate(user: User): void {
    log.info('User ' + user.toString() + ' created!');
  }

  @On(Event.ON_USER_CREATED)
  public onUserAfterCreate(user: User): void {
    log.info('User ' + user.toString() + ' created!');
  }
}
