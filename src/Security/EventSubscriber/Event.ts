export class Event {
  public static readonly ON_USER_BEFORE_CREATE = 'onUserBeforeCreate';
  public static readonly ON_USER_CREATED = 'onUserAfterCreate';
  public static readonly ON_USER_PASSWORD_CHANGED = 'onUserPasswordChanged';
}
