import { Service } from 'typedi';
import bcrypt from 'bcryptjs';
import { PasswordUpdaterInterface } from '.';
import { User } from '../User';

@Service()
export class PasswordUpdater implements PasswordUpdaterInterface {

    /**
     * Checks if the password is correct
     *
     * @param {User} user Current user
     * @param password The plain password
     *
     * @throws Error
     */
    public static async checkPassword(user: User, password: string): Promise<boolean> {
        return await bcrypt.compare(password, user.password);
    }

    /**
     * @inheritdoc
     */
    public async hashPassword(user: User): Promise<void> {
        const plainPassword: string | null | undefined = user?.plainPassword;
        try {
            if (!plainPassword) {
                return;
            }
            const hash: string = await bcrypt.hash(plainPassword, 10);
            user.password = hash;
            user.eraseCredentials();
        } catch (err) {
            console.log('Password cannot be encoded!', err.message);
            throw err;
        }
    }

    /**
     * @inheritdoc
     */
    public async changePassword(user: User, newPassword: string, currentPassword?: string | undefined): Promise<void> {
        if (currentPassword) {
            const isCurrentPasswordValid: boolean = await PasswordUpdater.checkPassword(user, currentPassword);
            if (!isCurrentPasswordValid) {
                throw new Error('Current password is invalid!');
            }
        }

        user.plainPassword = newPassword;
        await this.hashPassword(user);
    }
}
