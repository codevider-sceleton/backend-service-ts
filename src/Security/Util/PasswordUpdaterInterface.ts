import { User } from '../User';

export interface PasswordUpdaterInterface {
    /**
     * Updates the hashed password in the user when there is a new password.
     *
     * The implement should be a no-op in case there is no new password (it should not erase the
     * existing hash with a wrong one).
     *
     * @param {User} user
     *
     * @return void
     */
    hashPassword(user: User): Promise<void>;

    /**
     * Changes password in the user when there is a new password.
     *
     * The implement should be a no-op in case there is no new password (it should not erase the
     * existing hash with a wrong one).
     *
     * @param user
     * @param newPassword
     * @param currentPassword
     *
     * @return void
     */
    changePassword(user: User, newPassword: string, currentPassword?: string): Promise<void>;
}
