import { Service, Inject } from 'typedi';
import { UserManagerInterface } from '../UserManagerInterface';
import { User } from '../User';
import { UserManager } from '../UserManager';
import { EventDispatcher, EventDispatcherInterface } from '../../../lib/Decorators';
import { Event } from '../EventSubscriber/Event';

@Service()
export class UserManipulator {

  constructor(
    @Inject(type => UserManager) private userManager: UserManagerInterface,
    @EventDispatcher() private dispatcher: EventDispatcherInterface
  ) { }

  /**
   * Create a user. Carefully because the inputs are not validated here.
   *
   * @param username
   * @param password
   * @param email
   * @param enabled
   * @param superAdmin
   */
  public async create( username: string, password: string, email: string, enabled: boolean = true, superAdmin: boolean = false ): Promise<User> {
    const user = new User();
    user.username = username;
    user.plainPassword = password;
    user.email = email;
    user.enabled = enabled;
    user.setSuperAdmin(superAdmin);
    await this.userManager.save(user);

    this.dispatcher.dispatch(Event.ON_USER_CREATED, user);

    return user;
  }
}
