import { Command, InputInterface, InputOption } from '../../../lib/Command';
import { Inject } from 'typedi';
import { UserManipulator } from '../Util';

export class CreateUserCommand extends Command {

  @Inject()
  private userManipulator: UserManipulator;

  public configure(): void {
    this.setName('user:create');
    this.addOption('username', 'u', InputOption.VALUE_REQUiRED, 'Set user username.');
    this.addOption('email', 'e', InputOption.VALUE_REQUiRED, 'Set user username.');
    this.addOption('password', 'p', InputOption.VALUE_REQUiRED, 'Set user username.');
    this.addOption('disabled', 'd', InputOption.VALUE_NONE, 'Set user username.');
    this.addOption('super-admin', 's', InputOption.VALUE_NONE, 'Create user as super admin.');
  }

  public async execute(input: InputInterface): Promise<number> {
    await this.userManipulator.create(
      input.getOption('username') as string,
      input.getOption('password') as string,
      input.getOption('email') as string,
      input.getOption('disabled') as boolean,
      input.getOption('super-admin') as boolean
    );
    return 0;
  }
}
