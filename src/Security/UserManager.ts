import { Service, Inject } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { UserManagerInterface } from './UserManagerInterface';
import { PasswordUpdater, PasswordUpdaterInterface } from './Util';
import { UserRepository } from './Repository/UserRepository';
import { User } from './User';
import { UserNotFoundError } from './Error/UserNotFoundError';
import { ObjectID } from 'mongodb';

@Service()
export class UserManager implements UserManagerInterface {

    constructor(
        @OrmRepository() private userRepository: UserRepository,
        @Inject(type => PasswordUpdater) private passwordUpdater: PasswordUpdaterInterface
    ) { }

    /**
     * @inheritdoc
     */
    public getUser(): User {
        return new User();
    }

    /**
     * @inheritdoc
     */
    public async findUserById(id: string | number): Promise<User | undefined> {
        return await this.findUserBy({_id: new ObjectID(id)});
    }

    /**
     * @inheritdoc
     */
    public async findUserByUsername(username: string): Promise<User | undefined> {
        return await this.findUserBy({username});
    }

    /**
     * @inheritdoc
     */
    public async findUserByEmail(email: string): Promise<User | undefined> {
        return await this.findUserBy({ email });
    }

    /**
     * @inheritdoc
     */
    public async findUserByUsernameOrEmail(usernameOrEmail: string): Promise<User | undefined> {
        return await this.findUserBy({ usernameOrEmail });
    }

    /**
     * @inheritdoc
     */
    public async findUserByConfirmationToken(token: string): Promise<User | undefined> {
        return await this.findUserBy({ token });
    }

    /**
     * @inheritdoc
     */
    public async findUserBy(criteria: object): Promise<User | undefined> {
        const user = await this.userRepository.findOne(criteria);
        if (!user) {
            return undefined;
        }

        return user;
    }

    /**
     * @inheritdoc
     */
    public async findUsers(): Promise<User[]> {
        return await this.userRepository.find();
    }

    /**
     * @inheritdoc
     */
    public async deleteUser(id: number): Promise<void> {
        const user = await this.findUserById(id);
        if (!user) {
            throw new UserNotFoundError();
        }
        await this.userRepository.remove(user);
    }

    /**
     * @inheritdoc
     */
    // public async reloadUser(user: User): Promise<void> {
    // }

    /**
     * @inheritdoc
     */
    public async save(user: User, andSave: boolean = true): Promise<void> {
        await this.updatePassword(user);
        if (andSave) {
            await this.userRepository.save(user);
        }
    }

    /**
     * @inheritdoc
     */
    public async updatePassword(user: User): Promise<void> {
        await this.passwordUpdater.hashPassword(user);
    }
}
