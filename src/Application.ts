import 'reflect-metadata';

import { bootstrapMicroframework } from 'microframework';
import { Logger } from '../lib/Logger';
import { containerLoader, ormLoader, expressLoader, loggerLoader, monitorLoader, eventDispatchLoader }from '../lib/Modules';
import { banner } from '../lib/banner';

const log = new Logger();

bootstrapMicroframework({
  config: {
    showBootstrapTime: true,
    bootstrapTimeout: 5,
  },
  loaders: [
    loggerLoader,
    containerLoader,
    eventDispatchLoader,
    ormLoader,
    expressLoader,
    monitorLoader,
  ],
})
  .then(() => banner(log))
  .catch(error => log.error(`Application crashed: ${error}`));
