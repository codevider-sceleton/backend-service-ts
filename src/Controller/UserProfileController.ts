import { JsonController, Get, Post, Put, Param, Body, OnUndefined, OnNull, Delete } from 'routing-controllers';
import { Inject } from 'typedi';
// import IsAuthenticated from '../Middleware/Authentication';
import { UserManager, User } from '../Security';

@JsonController('profile')
export class UserProfileController {

  @Inject(type => UserManager)
  public readonly userManager: UserManager;

  @Get('/')
  public async profile(): Promise<User[]> {
    return await this.userManager.findUsers();
  }

  @Put('/edit')
  @OnNull(404)
  @OnUndefined(404)
  public async edit(@Param('id') id: string): Promise<User | undefined> {
    return await this.userManager.findUserById(id);
  }

  @OnUndefined(201)
  @Post('/change-password')
  public async changePassword(@Body({ required: true, validate: { groups: ['profile:changePassword'] } }) user: User): Promise<void> {
    await this.userManager.save(user);
  }
}
