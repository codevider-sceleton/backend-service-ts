import { JsonController, Get, Post, Put, Param, Body, OnUndefined, OnNull, Delete, Authorized } from 'routing-controllers';
import { Inject } from 'typedi';
import { UserManager, User } from '../Security';
import { UserNotFoundError } from '../Security/Error/UserNotFoundError';

@Authorized(User.ROLE_SUPER_ADMIN)
@JsonController('users')
export class UserController {

  @Inject(type => UserManager)
  public readonly userManager: UserManager;

  @Get()
  public async getCollection(): Promise<User[]> {
    return await this.userManager.findUsers();
  }

  @Get('/:id')
  @OnNull(404)
  @OnUndefined(404)
  public async getItem(@Param('id') id: string): Promise<User | undefined> {
    return await this.userManager.findUserById(id);
  }

  @OnUndefined(201)
  @Post()
  public async create(@Body({ required: true, validate: { groups: ['admin:create'] } }) user: User): Promise<void> {
    await this.userManager.save(user);
  }

  @Put('/:id')
  @OnUndefined(404)
  public async update(@Param('id') id: string, @Body({ validate: false }) user: User): Promise<User> {
    const userToUpdate = await this.userManager.findUserById(id);
    if (!userToUpdate) {
      throw new UserNotFoundError();
    }
    await this.userManager.save({ ...userToUpdate, ...user} as User);
    return { ...userToUpdate, ...user } as User;
  }

  @Delete('/:id')
  @OnUndefined(204)
  public async delete(@Param('id') id: number): Promise<void> {
    await this.userManager.deleteUser(id);
  }

  @Put('/add-roles/:id')
  public async addRole(@Param('id') id: string, @Body({required: true, validate: {groups: ['admin:roles']}}) payload: User): Promise<User> {
    const user = await this.userManager.findUserById(id);
    if (!user) {
      throw new UserNotFoundError();
    }
    for (const role of payload.roles) {
      user.addRole(role);
    }
    await this.userManager.save(user);

    return user;
  }
}
