import { Inject } from 'typedi';
import { JsonController, Body, Post } from 'routing-controllers';
import { User } from '../Security';
import { Authentication } from '../Security/Authentication/Authentication';

@JsonController()
export class AuthController {
    @Inject()
    private authService: Authentication;

    @Post('login')
    public async login(@Body({ required: true, validate: { groups: ['login'] } }) credentials: User): Promise<{ token: string }> {
      const token = await this.authService.authenticate(credentials);
      return { token };
    }

    @Post('sign-up')
    public async register(@Body({ required: true, validate: { groups: ['registration'] } }) payload: User): Promise<User> {
      return await this.authService.register(payload);
    }
}
