FROM node:alpine

RUN mkdir -p /srv/app

WORKDIR /srv/app

COPY package.json package-lock.json ./

RUN npm install

COPY . ./

EXPOSE 4001

CMD npm run dev